package com.adityajhaver.demo.androiddemoexamples.sqlitedemo;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.adityajhaver.demo.androiddemoexamples.R;

import java.util.ArrayList;

public class ParsingActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();
    private ListView lv;
    private Context context;

    // URL to get contacts JSON
    private String url1 = "https://api.androidhive.info/contacts/";

    //ArrayList<HashMap<String, String>> contactList;
    ArrayList<Contact> contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parsing);

        contactList = new ArrayList<>();

        lv = (ListView) findViewById(R.id.list);
        context = ParsingActivity.this;
        new GetContacts(context, url1, handler).execute();

    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.obj.toString().equalsIgnoreCase("success")) {
                ListCustomAdapter adapter = new ListCustomAdapter(context,
                        new DatabaseHandler(context).getAllContacts());
                lv.setAdapter(adapter);
            }
        }
    };


    /**
     * Async task class to get json by making HTTP call
     */
    /*private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ParsingActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url1);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray contacts = jsonObj.getJSONArray("contacts");

                    // looping through All Contacts
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);

                        String id = c.getString("id");
                        String name = c.getString("name");
                        String email = c.getString("email");
                        String address = c.getString("address");
                        String gender = c.getString("gender");

                        // Phone node is JSON Object
                        JSONObject phone = c.getJSONObject("phone");
                        String mobile = phone.getString("mobile");
                        String home = phone.getString("home");
                        String office = phone.getString("office");

                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();

                        // adding each child node to HashMap key => value
                        contact.put("id", id);
                        contact.put("name", name);
                        contact.put("email", email);
                        contact.put("mobile", mobile);

                        // Contact object data
                        Contact contactObj = new Contact(name, mobile, email);

                        // adding contact to contact list
                        contactList.add(contactObj);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            *//**
     * Updating parsed JSON data into ListView
     * *//*
            *//*ListAdapter adapter = new SimpleAdapter(
                    ParsingActivity.this, contactList,
                    R.layout.list_item, new String[]{"name", "email",
                    "mobile"}, new int[]{R.id.name,
                    R.id.email, R.id.mobile});
*//*
            lv.setAdapter(adapter);
        }

    }*/


}
