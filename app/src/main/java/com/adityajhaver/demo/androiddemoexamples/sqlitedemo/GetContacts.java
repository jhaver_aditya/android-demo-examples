package com.adityajhaver.demo.androiddemoexamples.sqlitedemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Aditya Jhaver on 07-01-2018.
 */

public class GetContacts extends AsyncTask<Void, Void, Void> {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private ProgressDialog pDialog;
    private String url;
    private ArrayList<Contact> contactList;
    private Handler handler;

    public GetContacts(Context context, String url, Handler handler) {
        this.url = url;
        this.context = context;
        this.handler = handler;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        contactList = new ArrayList<>();
        // Showing progress dialog
/*        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();*/

    }

    @Override
    protected Void doInBackground(Void... arg0) {
        HttpHandler sh = new HttpHandler();

        // Making a request to url and getting response
        String jsonStr = sh.makeServiceCall(url);

        Log.e(TAG, "Response from url: " + jsonStr);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);

                // Getting JSON Array node
                JSONArray contacts = jsonObj.getJSONArray("contacts");

                // looping through All Contacts
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);

                    String id = c.getString("id");
                    String name = c.getString("name");
                    String email = c.getString("email");
                    String address = c.getString("address");
                    String gender = c.getString("gender");

                    // Phone node is JSON Object
                    JSONObject phone = c.getJSONObject("phone");
                    String mobile = phone.getString("mobile");
                    String home = phone.getString("home");
                    String office = phone.getString("office");

                    // tmp hash map for single contact
                    HashMap<String, String> contact = new HashMap<>();

                    // adding each child node to HashMap key => value
                    contact.put("id", id);
                    contact.put("name", name);
                    contact.put("email", email);
                    contact.put("mobile", mobile);

                    // Contact object data
                    Contact contactObj = new Contact(name, mobile, email);

                    // adding contact to contact list
                    contactList.add(contactObj);
                }
            } catch (final JSONException e) {
                Log.e(TAG, "Json parsing error: " + e.getMessage());
            }
        } else {
            Log.e(TAG, "Couldn't get json from server.");
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        // Dismiss the progress dialog
/*
        if (pDialog.isShowing())
            pDialog.dismiss();
*/
        /**
         * Updating parsed JSON data into ListView
         * */
        DatabaseHandler dHandler = new DatabaseHandler(context);

        if(dHandler.getContactsCount()>0) {
            dHandler.deleteAllContacts();
        }

        dHandler.addAllContacts(contactList);

        Message message = new Message();
        message.obj = "Success";

        handler.sendMessage(message);
    }

}

