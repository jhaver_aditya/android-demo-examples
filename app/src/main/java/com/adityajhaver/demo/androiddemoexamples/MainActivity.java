package com.adityajhaver.demo.androiddemoexamples;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.adityajhaver.demo.androiddemoexamples.contentproviderdemo.ContactListActivity;
import com.adityajhaver.demo.androiddemoexamples.sqlitedemo.Contact;
import com.adityajhaver.demo.androiddemoexamples.sqlitedemo.DatabaseHandler;
import com.adityajhaver.demo.androiddemoexamples.sqlitedemo.ParsingActivity;
import com.adityajhaver.demo.androiddemoexamples.userattentiondemo.UserAttentionActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 111;
    private final String TAG = getClass().getSimpleName();
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = MainActivity.this;
    }

    /**
     * Content Provider Demo
     */
    public void clickOnButton1(View view) {

    }

    /**
     * Using Option Menu, Context Menu Demo
     */
    public void clickOnButton2(View view) {

    }

    /**
     * Getting the User’s Attention Demo
     * 1. Spinner
     * 2. Progress Bar
     * 3. Generating Alert Dialog
     * 4. Toast
     */
    public void clickOnButton3(View view) {
        Intent i = new Intent(this, UserAttentionActivity.class);
        startActivity(i);
    }

    /**
     * Building Background-Aware Applications Demo
     * 1. AsyncTask
     * 2. runOnUIThread
     * 3. Timer
     */
    public void clickOnButton4(View view) {

    }

    /**
     * Working with Database Demo
     * 1. Creating a database
     * 2. Opening and closing a database
     * 3. Working with Inserts, updates and deletes
     * 4. Parse URL (XML and JSON)
     */
    public void clickOnButton5(View view) {
        //Sqlite db related
        sqliteDemo();
    }

    /**
     * Integrating with core services
     * 1. Integrate Android app with address book, Email, SMS,Phone Call, Map
     * 2. Playing Media (Audio and Video)
     * 3. Using the Photo Library and Camera
     */
    public void clickOnButton6(View view) {
        //retrivingContactDemo();

        // For Opening Contacts
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        //startActivity(intent);

        //For Email
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"jhaver.aditya@gmail.com"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Demo");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hello!!!");

        /* Send it off to the Activity-Chooser */
        //startActivity(Intent.createChooser(emailIntent, "Send mail..."));

        // for SMS
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        //smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", "12125551212");
        smsIntent.putExtra("sms_body", "Body of Message");
        //startActivity(smsIntent);

        Intent dailIntent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("phone:9865321245"));
        //startActivity(dailIntent);

        //for Map
        // Create a Uri from an intent string. Use the result to create an Intent.
        Uri gmmIntentUri = Uri.parse("google.streetview:cbll=46.414382,10.013988");

        // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        // Make the Intent explicit by setting the Google Maps package
        mapIntent.setPackage("com.google.android.apps.maps");

        // Attempt to start an activity that can handle the Intent
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            //startActivity(mapIntent);
        }

        int permissionCheck = ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.CAMERA);

        // for camera

        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
        }

        /*if (permissionCheck == 0) {
            Intent camIntent = new Intent("android.media.action.IMAGE_CAPTURE");
            startActivity(camIntent);
        } else {
//            Toast.makeText(mContext, "Please give the camera permission!!!", Toast.LENGTH_SHORT).show();
            //askPermission();
        }*/

    }


    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                } else {
                    //not granted
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    /**
     * parsing Activity
     */
    public void clickOnButton7(View view) {
        Intent i = new Intent(mContext, ParsingActivity.class);
        startActivity(i);
    }

    /**
     *
     * */
    public void clickOnButton8(View view) {

    }

    /**
     *
     * */
    public void clickOnButton9(View view) {

    }

    // sqlite demo
    private void sqliteDemo() {
        DatabaseHandler db = new DatabaseHandler(mContext);

        db.addContact(new Contact("Aditya", "9100000000", ""));
        db.addContact(new Contact("Mahesh", "9122222222", ""));
        db.addContact(new Contact("Pawan", "9133333333", ""));
        db.addContact(new Contact("Nitesh", "9144444444", ""));
        db.addContact(new Contact("Vivek", "9155555555", ""));
        db.addContact(new Contact("Shridhar", "9166666666", ""));
        db.addContact(new Contact("Shrikant", "9177777777", ""));

        List<Contact> contacts = db.getAllContacts();

        for (Contact cn :
                contacts) {
            Log.e(TAG, "sqliteDemo: " + cn.toString());
        }
    }

    // retriving contact demo
    private void retrivingContactDemo() {
        Intent intent = new Intent(mContext, ContactListActivity.class);
        startActivity(intent);
    }
}
