package com.adityajhaver.demo.androiddemoexamples.sqlitedemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.adityajhaver.demo.androiddemoexamples.R;

import java.util.ArrayList;

/**
 * Created by Aditya Jhaver on 07-01-2018.
 */

public class ListCustomAdapter extends ArrayAdapter<Contact> {

    private ArrayList<Contact> contacts;
    private Context context;

    public ListCustomAdapter(Context context, ArrayList<Contact> contacts) {
        super(context, R.layout.list_row_item, contacts);
        this.context = context;
        this.contacts = contacts;
    }

    private static class ViewHolder {
        TextView name, email, phone;
    }

    private static int lastPosition = -1;

    public View getView(int position, View convertView, ViewGroup parent) {
        Contact contact = contacts.get(position);

        ViewHolder viewHolder;

        View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_row_item, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.tvContactName);
            viewHolder.email = (TextView) convertView.findViewById(R.id.tvContactEmail);
            viewHolder.phone = (TextView) convertView.findViewById(R.id.tvContactPhone);

            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.name.setText(contact.get_name());
        viewHolder.email.setText(contact.get_email());
        viewHolder.phone.setText(contact.get_phone());

        return result;
    }


}
