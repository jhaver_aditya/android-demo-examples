package com.adityajhaver.demo.androiddemoexamples.sqlitedemo;

/**
 * Created by Aditya Jhaver on 07-01-2018.
 */

public class Contact {

    private int _id;
    private String _name, _phone, _email;

    public Contact() {
    }

    public Contact(String _name, String _phone, String _email) {
        this._name = _name;
        this._phone = _phone;
        this._email = _email;
    }

    public Contact(int _id, String _name, String _phone) {
        this._id = _id;
        this._name = _name;
        this._phone = _phone;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_phone() {
        return _phone;
    }

    public void set_phone(String _phone) {
        this._phone = _phone;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "_id=" + _id +
                ", _name='" + _name + '\'' +
                ", _phone='" + _phone + '\'' +
                ", _email='" + _email + '\'' +
                '}';
    }
}

